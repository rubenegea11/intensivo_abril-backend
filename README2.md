# dependencias 

express: framework que nos permite realizar un correcto enrutamiento de las órdenes y peticiones    

dotenv: para configurar variables de entorno de un proyecto 

typescript: para la transpilación de typescript 
a JS

concurrently: ejecución concurrente del proceso daemom para la actualizacion automatica 

webpack:

eslint:
jest:

# scripts de NPM

start:local : ejecuta el archivo index.js, donde tenemos la instanciación de nuestro proyecto y el puerto en el que se ejecuta
(Posteriormente es borrado, ya que sustituimos el .js por el .ts)

build: reemplaza a start:local; ejecuta tsc (de forma local) para generar el index.js con el enrutado 

start: ejecuta la aplicación 

dev: activa el proceso demon  para actualizar constantemente el localhost cada vez que guardemos los cambios en el proyecto