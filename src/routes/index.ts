/**
 * Root Router
 * Redirections to Routers
 */

import express, { Request, Response } from 'express';
import helloRouter from './HelloRouter';
import { LogInfo } from '@/utils/logger';

//Server instance
let server = express();

//router instance
let rootRouter = express.Router();

//Activate for request to http://localhost:8000/api

// GET http://localhost:8000/api/
rootRouter.get('/', (req: Request, res: Response) => {
    LogInfo('GET: http://localhost:8000/api')
    //Send hello world
    res.send('Welcome to my API Restful: Express + TS + Node + NoDemon...')
});

//Redirections to Routers 

server.use('/', rootRouter);    //http://localhost:8000/api/
server.use('/hello', helloRouter); //http://localhost:8000/api/hello
//Add more routes

export default server;